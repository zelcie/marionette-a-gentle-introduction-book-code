ContactManager.module("ContactsApp.New", function(New, Contactmanager, Backbone, Marionette, $, _){

    New.Contact =  Contactmanager.ContactsApp.Common.Views.Form.extend({
        title: "New Contact",

        onRender: function(){
            this.$(".js-submit").text("Create contact");
        }
    });

});